/**
 * Likes.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  schema:true,

  attributes: {
    id:{
      type:'string',
      primaryKey: true,
      required: true,
      unique: true
    },
    name:{
      type:'string',
      required: true
    },
    users:{
      type: 'array',
      defaultsTo: []
    }
  },

  // Lifecycle Callbacks
  beforeCreate: function (values, cb) {

    // Remove User email
    if(values.user_email){
      values.users.push({email:values.user_email});
      delete values.user_email;
    }
    cb();

  },

  beforeUpdate: function (valuesToUpdate, cb) {
    // Remove User email
    if(valuesToUpdate.user_email){

    	Likes.findOne({
  			id:valuesToUpdate.id
  		}).exec(function (err, like){
  			if (err) {
  				return res.serverError(err);
  			}
  			if (!like) {
          //Ideally I am supposed to create a record here
  				return;
  			}

      //Get the old record and fetch users who already liked those
      var found = _.find(like.users, function(item){return item.email === valuesToUpdate.user_email});
      if(found){
        return;
      }
      like.users.push({email:valuesToUpdate.user_email})
      valuesToUpdate.users = like.users;

      delete valuesToUpdate.user_email;

      cb();
    })

    return;
   }

    cb();
  },

  findLikesbyEmail: function(opts, cb){
    var email = opts.email;
    Likes.native(function(err, collection){
      if(err){ return cb(err,null);}

      collection.find({'users.email': email}).toArray(function(err,results){
        if(err){ return cb(err, null);}

        return cb(null, results);
      });
    })
  },

  SuggestFeature: function(opts, cb){
    var featureName = opts.feature;
    var email = opts.email;

    Likes.native(function(err, collection){
      if(err){ return cb(err,null);}

      collection.find({'users.email': email}).map(function(x){
        if(x.feature){
            return x.feature[featureName];
        }else{
          return x.feature = [];
        }
      }).toArray(function(err,results){
          if(err){ return cb(err, null);}

          //Flatten the array
          var results = [].concat.apply([], results);

          //Group the results
          var temp = {};
          var obj = null;
          for(var i=0; i < results.length; i++) {
             obj=results[i];
             if(obj==null){
               continue;
             }
             if(!temp[obj.value]) {
                 temp[obj.value] = obj;
             } else {
                 temp[obj.value].user_count += obj.user_count;
             }
          }
          results = [];
          for (var prop in temp)
              results.push(temp[prop]);

          //Find confidence
          var total = 0;
          results.forEach(function(a){total+=a.user_count});

          results.forEach(function(a){
            a.confidence = a.user_count/total*100;
            delete a.user_count;
          });

          return cb(null, results);
        });
      });
  },

  clearAllFeatures : function(opts, cb){
    var force = opts.force;

    if(force != 'strong'){
      return cb(null, {"message" : "force should be strong to do this!"});
    }

    var featureArr={};

    Feature.find().exec(function(err, allFeatures){
      //Go through all features
      for(i=0;i<allFeatures.length;i++){
        var indifeature = allFeatures[i].name;
        featureArr[indifeature] =[];

        //Go through all values of each feature
        for(j=0;j<allFeatures[i].values.length;j++){
          featureArr[indifeature].push({'value':j,'user_count':0})
        }
      }
      Likes.native(function(err, collection){
        if(err){ return cb(err,null);}

        var bulk = collection.initializeUnorderedBulkOp();
        bulk.find( {} ).update( { $set: { feature : featureArr } } );
        bulk.execute();

        return cb(null, {"message" : "All features cleared!"});
      });
    });


  }

};
