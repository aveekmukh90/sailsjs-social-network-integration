/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    email:{
      type:'email',
      primaryKey: true,
      required: true,
      unique: true
    }
  },


  populateFeaturesinLikes : function(opts, cb){
    //Populates the feature of a single user by email
    var email = opts.email;
    User.findOne({email:email}).exec(function(err, usr){
      if(err){ return cb(err,null);}

      if(!usr){
        return cb(null, {"message" : "Error: No such user found"});
      }

      if(!usr.feature){
        return cb(null, {"message" : email+"Nothing to do! No feature of this user"});
      }

      //Todo : I should ideally let the database do this
      Likes.find({'users.email': email}).exec(function(err, allLikes){

        for(i=0;i<allLikes.length;i++){
          curLike = allLikes[i];
          newFtr = curLike.feature;

          for(j=0;j<usr.feature.length;j++){
            if(usr.feature[j].is_final != true){
              //console.log(usr.feature[j].is_final);
              //console.log("Skipped");
              continue;
            }

            ftrname = usr.feature[j].name;
            ftrval = usr.feature[j].value;

            newFtr[ftrname].forEach(function(elem){if(elem.value == ftrval){elem.user_count++;}});
          }

          //console.log(email);
          //console.log(curLike.name);
          //console.log(newFtr);

          Likes.native(function(err, collection){
            if(err){ console.log("Error User:populateFeaturesinLikes ");return cb(err,null);}
            collection.update({name:curLike.name},{ $set: { feature : newFtr } });
            }
          )

        }
      });

      return cb(null, {"message" : email+" User's features populated!"});
    })
  },

  populateFeaturesinLikesAllUsers : function(opts, cb){

    var resultG = [];
    User.find().exec(function(err, allUsers){
      for(i=0;i<allUsers.length;i++){
        emailu = allUsers[i].email;
        User.populateFeaturesinLikes({'email':emailu},function(err, res){
          if(err){return cb(err,null)}

          console.log(res);
          //resultG.push(res);
        });

        resultG.push({"message": emailu +" User's features populated!"});
      }

      return cb(null, resultG);
    });


  }
};
