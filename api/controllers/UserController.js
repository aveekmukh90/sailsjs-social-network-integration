/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Linkedin = require('machinepack-linkedin');

module.exports = {

	// linkedin: function(req, res){
	// 	console.log("I reached linkedin");
	//
	// 	// Creates the login url used to gain acccess to their LinkedIn account.
	// 	Linkedin.getLoginUrl({
	// 	client_id: 'cno31ufqrp03',
	// 	redirecturl: 'https://localhost:1337/linkedin',
	// 	scope: 'r_fullprofile%20r_emailaddress%20w_share',
	// 	}).exec({
	// 	// An unexpected error occurred.
	// 	error: function (err){
	// 	  console.log("Seems failure");
	// 	  console.log(err);
	// 	},
	// 	// OK.
	// 	success: function (){
	// 	  console.log("Seems success");
	// 	},
	// 	});
	// },


	create: function(req, res){
			User.findOne({
			  email:req.param('email')
			}).exec(function (err, usr){
			  if (err) {
			    return res.serverError(err);
			  }
			  if (!usr) {
			    //We need to create the User here
					User.create(req.params.all()).exec(function (err, usr){
					  if (err) { return res.serverError(err); }

					  return res.json(usr);
					});

					return;
			  }

			//Update the user here with extra fields
			User.update(usr,req.params.all()).exec(function afterwards(err, updated){
				  if (err) {
				    // handle error here- e.g. `res.serverError(err);`
				    return;
				  }

					return res.json(updated);
				});

			});
		},

	populateFeaturesinLikes: function(req, res){
		var email = req.param('email');

		User.populateFeaturesinLikes({'email':email}, function(err, results){
			if(err){return res.serverError(err);}

			return res.ok(results);
		})
	},

	populateFeaturesinLikesAllUsers: function(req, res){
		User.populateFeaturesinLikesAllUsers({}, function(err, results){
			if(err){return res.serverError(err);}

			return res.ok(results);
		})
	}
};
