/**
 * LikesController
 *
 * @description :: Server-side logic for managing likes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: function(req, res){
		Likes.findOne({
			id:req.param('id')
		}).exec(function (err, like){
			if (err) {
				return res.serverError(err);
			}
			if (!like) {
				//I am supposed to create a record here
				Likes.create(req.params.all()).exec(function (err,created){
					if(err){ return res.serverError(err); }
					//return res.json(created);
				})
			}

			//console.log("I am here because the value exists, I will try to update it");
			Likes.update({id:req.param('id')},req.params.all()).exec(function after(err, updated){
				if(err){
					return res.serverError(err);
				}
				//console.log("Should have been updated");
				return res.json(updated);
			});

			//return res.ok();
	})
	},

	findLikesbyEmail : function(req, res){
		var email = req.param('email');
		Likes.findLikesbyEmail({'email':email}, function(err, results){
			if(err){return res.serverError(err);}

			return res.ok(results);
		});
	},

	SuggestFeature : function(req, res){
		var email = req.param('email');
		var feature = req.param('feature');
		Likes.SuggestFeature({'email':email,'feature':feature}, function(err, results){
			if(err){return res.serverError(err);}

			return res.ok(results);
		});
	},

	clearAllFeatures : function(req,res){
		var force = req.param('force');

		Likes.clearAllFeatures({'force':force}, function(err, results){
			if(err){return res.serverError(err);}

			return res.ok(results);
		})
	}

};
