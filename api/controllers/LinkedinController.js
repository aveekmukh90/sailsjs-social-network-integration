/**
 * LinkedinController
 *
 * @description :: Server-side logic for managing linkedins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Http = require('machinepack-http');
var Linkedin = require('machinepack-linkedin');

module.exports = {

	success : function(req, res){

		console.log(req.params.all());

		err = req.param('error');
		if(err){
			console.log(err);
			return res.ok(err);
		}

		code = req.param('code');
		if(code){
			furl = 'https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code';
			furl = furl + '&code='+code;
			furl = furl + '&redirect_uri=http://localhost:1337/linkedin/success';
			furl = furl + '&client_id=cno31ufqrp03&client_secret=TlbQvT4bazrr2RQv';

			//console.log(furl);

			// Send an HTTP request and receive the response.
			Http.sendHttpRequest({
			url: furl,
			method: 'post',
			headers: {'Content-Type': 'application/x-www-form-urlencoded'},
			}).exec({
			// An unexpected error occurred.
			error: function (err){
				console.log(err);
			},
			// 404 status code returned from server
			notFound: function (result){
				console.log("not found");
			},
			// 400 status code returned from server
			badRequest: function (result){
				console.log("bad req");
				console.log(result);
			},
			// 403 status code returned from server
			forbidden: function (result){
				console.log("forb");
			},
			// 401 status code returned from server
			unauthorized: function (result){
				console.log("unauth");
			},
			// 5xx status code returned from server (this usually means something went wrong on the other end)
			serverError: function (result){
				console.log("serv fail");
			},
			// Unexpected connection error: could not send or receive HTTP request.
			requestFailed: function (){
				console.log("req fail");
			},
			// OK.
			success: function (result){
				console.log("success, access token obtained!");
				res = JSON.parse(result.body);
				res = JSON.parse(res);
				//console.log(res.access_token);
				sails.config.appsettings.linkedin_access_token = res.access_token;
				//console.log(sails.config.appsettings.linkedin_access_token);

				lurl = '/v1/people/~'+'?oauth2_access_token='+res.access_token+'&format=json';
				console.log(lurl);
				// Obtains the profile from the authorized user.
				Http.sendHttpRequest({
					baseUrl: 'https://api.linkedin.com',
					url: lurl,
					method: 'get',
					params:
					{
						'Authentication': 'Bearer '+res.access_token

					},
					headers:
					{
						'Content-Type' : 'application/x-www-form-urlencoded',
					}

				}).exec({
					success: function(response)
					{
						console.log(response.body);
					},
					error: function(err)
					{
						console.log("Error");
						console.log(err);
					}
				});

			},
			});
		}


		return res.redirect('/linkedin');
	}

};
